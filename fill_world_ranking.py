import sqlite3


def main():
    conn = sqlite3.connect("world_cup.db")
    c = conn.cursor()
    c.execute("CREATE TABLE IF NOT EXISTS world_ranking (name text not null, rank int not null)")

    while True:
        line = input().split()
        if line[0] == "insert":
            conn.execute("INSERT INTO world_ranking VALUES ('{}', {})".format(line[1], line[2]))

        if line[0] == "delete":
            conn.execute("DELETE FROM world_ranking WHERE name = {}".format(line[1]))

        if line[0] == "end":
            conn.commit()
            c.close()
            conn.close()
            break

if __name__ == "__main__":
    main()
