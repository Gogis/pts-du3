## Zadanie
Domácu úlohu odovzdajte ako GITovský repozitár e-mailom na relatko@gmail.com. Do subjectu napiste PTS DU2. Nezabudnite napisat svoje meno. Termín na odovzdanie úlohy je 27.4.2017 23:00

Úlohou je navrhnúť systém, ktorý simuluje proces kvalifikácie na a samotné majstrovstvá sveta vo futbale 2018. Svoj dizajn popíšte. Urobte čiastočnú implemntáciu, ktorá bude simulovať niejaký iný, vymyslený, zjednodušený turnaj. Implementácia nemusí (a ani to neočakavam) byť postačujúca na simuláciu klalifikácie a MS, avšak dizajn má byť dostatočne flexibilný aby implementácia ostatných featurov nebola zásadným problémom vyžadujúcim kompletnú zmenu designu.
Systém sa nainicializuje do stavu zodpovedajúcemu 1.1.2015. Systém spolupracuje s dvoma inými podsystémami: FIFA World Ranking a Simulátor zápasov (týmito systémami sa nemáte zaoberať, máte však definovať ich interface a na čiastkovú impleméntáciu budete potrebovať ich stuby - triviálne nakódené verzie).

Systém má dva use casy.
Posunuť sa o deň vpred
Systém odsimuluje (s pomocou simulátora zápasov a FIFA World Ranking) možný vývoj počas nasledujúceho jedného dňa a vytvorí nový stav turnaja. Táto simulácia zahŕňa: zaznamenanie výsledkov zápasov, zmenu tabuliek, žrebovanie tímov do jednotlivých skupín (nasadzovanie do žrebovania určuje FIFA World Ranking), postup tímov medzi rôznymi časťami súťaže.
Zistiť aktuálny stav turnaja
Systém vie poskytnúť klientovy aktualny stav sútaže (výsledky historických zápasov, aktuálne tabuľky, tabuľky už ukončených častí turnaja).
Pre všeobecný prehľad ako prebieha kvalifikácia na MS vo futbale si pozrite Wikipediu. Pre čo najrýchlejšie pochopenie potenciálnej zlošitosti kvalifikačného procesu odporúčam si pozrieť si ako to prebieha v Ázii.

Ak vám niečo v zadaní chýba, alebo sa vám niečo veľmi nepáči, kľudom si ho obmeňte.

## Strucna dokumentacia
+ System World Ranking pracuje s databazou hodnoteni svetovych reprezentacii. Pri inicializacii sa
cela tabulka nahra do pamate, po nej sa da volne upravovat a aj zapisat do databazy.
+ System Match Simulator obsahuje len je jednoduchu funkciu, ktora vrati nahodny vysledok zapasu, s
distribuciou pravdepodovnosti rozlozenou podla prislusneho ranku teamov vo World Ranking. Vysledkom
zapasu su zatial len dva mozne golove stavy a to 0:1 a 1:0.

Na simulaciu kvalifikacie a turnaja su vytvorene classy Qualification, resp. Tournament. Vstupne
parametre pre kvalifikaciu su num_teams, num_groups, num_advancers. Do kvalifikacie sa vzdy dostava
hornych num_teams teamov z World Rankingu, nasledne su rozdeleni do num_groups skupin (kazda o
velkosti num_teams/num_groups). Nasledne sa vytvori plan zapasov. Plan sa rozvrhuje tak, aby sa v
jeden den v kazdej skupine odohral zapas medzi x-tym a y-tym teamom. Kvalifikacne zapasy koncia vo
chvili, ked kazdy team odohral zapas s kazdym teamom v jeho skupine. Vysledky zapasov su
zaznamenavane a daju sa vypisat pomocou funkcie matches_history(). Stav jednotlivych skupin, v
ktorych su teamy zoradene podla poctu bodov sa da vypisat pomocou funkcie print_groups(). System
kvalifikacie predpoklada, ze vo World rankingu je aspon num_teams teamov, num_teams je parne cislo,
num_groups deli num_teams, num_teams/num_groups >= num_advancers a
num_teams/num_groups\*num_advancers je mocnina dvojky.

Na samotny turnaj je vytvoreny system Tournament. Vstupnym parametrom je Tournament je zoznam
kvalifikovanych teamov zoradenych v zostupnom poradi podla dosiahnuteho poctu bodov v kvalifikacii.
Turnaj nasledne funguje ako 'pavuk', teda v kazdy den sa vyberie dvojica teamov z opacnych koncov
zoznamu aktualnych teamov a odsimuluje sa medzi nimi zapas. Vitaz postupuje do dalsieho kola,
prehravajuci team je eliminovany z turnaja. Potom, ako kazdy team odohra svoj zapas so svojim
"komplementarnym" sa system presunie do dalsieho kola, v ktorom sa proces opakuje s vitaznymi timami
z minuleho kola. Pocet kol je rovny dvojkovemu logaritmu z kvalifikovanych teamov, aby na konci
zostal len vitaz. Rovnako ako v kvalifikacii, aj v turnaji sa daju vypisat vysledky vsetkych do
daneho dna odohratych zapasov pomocou funkcie match_history(). Zapas aktualneho dna sa da vypisat
pomocou fcie print_todays_match a vsetky zapasy aktualneho kola (vratane este neodohratych) sa daju
vypisat pomocou funkcie print_round_matches. Kompletna tabulka teamov sa da vypisat zavolanim fcie
print_final_results().

## Moj jednoduchy turnaj
Do kvalifikacie je vybranych hornych 40 teamov podla World Rankingu. Timy su nasledne rozdelene do
8 skupin po 5 teamov. V kazdy sutazny den odohra v kazdej skupine niektora dvojica (s rovnakymi
indexami) zapas. Vitaz vzdy dostane 1 bod, prehravajuci 0 bodov. Po skonceni skupinovej fazy, ked
kazdy tim bude bude mat odohraty zapas s kazdym dalsim v jeho skupine, sa z kazdej skupiny vyberu 2
timy s najvyssim poctom bodov, ktori postupuju do turnaja - teda turnaj zacina so 16 timami. Turnaj
prebieha podla vyssie popisanych pravidiel, v kazdom kole polovica timov vypadne a polovica timov
postupi do dalsieho. Prehravajuci tim je vzdy pridany navrch do vyslednej tabulky turnaja (ktora
nijako inak nie je determinovana). Po finale je tam na prve miesto pridany aj vitaz.
