import match_simulator
import world_ranking
import sqlite3

def get_balanced_groups(teams, num_groups):
    group_size = len(teams)//num_groups
    groups = []

    for i in range(num_groups):
        group = []
        for j in range(i, len(teams), num_groups):
            # (team_name, points, world_rank)
            group.append( (teams[j][1], 0, teams[j][0]) )
        groups.append(group)

    return groups


def get_match_schedule(group_size, num_groups):
    """Create match schedule so that each team will have a match with every team
        in his own group and also no team will have two matches in the same day."""
    matches = []

    for i in range(group_size):
        for j in range(i+1, group_size):
            for k in range(num_groups):
                matches.append( (k, i, j) )

    return matches


class Qualification:
    """Pick best num_teams in the World Ranking to qualification, divide them into num_groups,
        out of each group num_advancers advance to tournament."""

    def __init__(self, num_teams, num_groups, num_advancers):
        self.num_groups = num_groups
        self.num_advancers = num_advancers
        self.matches_completed = 0

        self.groups = []
        self.matches = []
        self.match_results = []
        ranking = world_ranking.WorldRanking()
        teams = []

        for team, rank in ranking.team_ranking.items():
            teams.append( (int(rank), team) )
            if len(teams) == num_teams: break
        
        # sorted by highest world_rank
        teams = sorted(teams)[::-1]
        self.group_size = len(teams)//self.num_groups

        self.groups = get_balanced_groups(teams, self.num_groups)
        self.matches = get_match_schedule(self.group_size, self.num_groups)

    def next_day(self):
        """Simulate today's matches in each group and record the results."""
        if self.matches_completed == len(self.matches): return 0
        #print(self.matches_completed, len(self.matches))

        # simulate todays matches
        for i in range(self.matches_completed, self.matches_completed+self.num_groups):
            gid = self.matches[i][0]
            team1_id = self.matches[i][1]
            team2_id = self.matches[i][2]
            #print(gid, team1_id, team2_id)

            res = match_simulator.get_result(
                self.groups[gid][team1_id], self.groups[gid][team2_id])

            self.match_results.append(res)
            res = res[0]

            # add points to the winner
            winner = team1_id if res == 1 else team2_id
            self.groups[gid][winner] = (self.groups[gid][winner][0],
                self.groups[gid][winner][1] + 1, self.groups[gid][winner][2])

        self.matches_completed += self.num_groups

        return -1
    
    def get_advancers(self):
        """Return a list of advancers sorted by highest numer of points."""
        advancers = []
        for group in self.groups:
            group = sorted(group, key=lambda x: x[1])[::-1]

            for i in range(self.num_advancers):
                advancers.append(group[i])

        # sort advancers by their points scored in qualification
        advancers = sorted(advancers, key=lambda x: x[1])[::-1]
        return advancers

    def matches_history(self):
        """Print all matches played until today (excluding today's match)."""
        for i in range(self.matches_completed):
            team1 = self.groups[self.matches[i][0]][self.matches[i][1]][0]
            team2 = self.groups[self.matches[i][0]][self.matches[i][2]][0]
            result = self.match_results[i]
            print("{:>12} {}:{} {}".format(team1, result[0], result[1], team2))

    def print_groups(self):
        """Sort teams in each group by the highest number of points and print them."""
        for group in self.groups:
            group = sorted(group, key=lambda x: x[1])[::-1]

            for x in range(len(group)):
                print("{}. {} ({} pts)".format(x+1, group[x][0], group[x][1]))
            print()
