import sqlite3

class WorldRanking:
    def __init__(self):
        self.team_ranking = {}
        conn = sqlite3.connect("world_cup.db")
        cursor = conn.execute("SELECT name, rank FROM world_ranking")

        for row in cursor:
            self.team_ranking[row[0]] = int(row[1])

    def write_to_db():
        conn = sqlite3.connect("world_cup.db")
        for team, rank in team_ranking.items():
            conn.execute("SELECT " + team + ", " + str(rank), "FROM world_ranking")
            if conn.fetchone():
                conn.execute("UPDATE world_rankings set rank = {} WHERE name = {}"
                    .format(rank, team))
            else:
                conn.execute("INSERT INTO world_rankings (name, rank) VALUES ({}, {})"
                    .format(team, rank))
