import match_simulator

class Tournament:
    """ Assume that teams are passed sorted in the descending order by the number of points
        they scored in qualification. Simulate the tournament by each day picking two teams
        from the opposite ends of the team list. When the round is done, e.g. each team has
        played a match with it's complementary team on the other end, repeat the process with
        the remaining teams unil only the winning team remains."""

    def __init__(self, teams):
        self.teams = teams
        self.num_teams = len(teams)
        self.matches_history = []
        self.teams_next = []
        self.final_results = []
        self.match_counter = 0

    def next_day(self):
        """Simulate todays' match, save the result and move the winner into next round."""
        # if each team has already played its match in this round
        if self.match_counter == self.num_teams//2:
            self.teams = self.teams_next
            self.teams_next = []
            self.num_teams //= 2
            self.match_counter = 0

        team1 = self.teams[self.match_counter]
        team2 = self.teams[self.num_teams - 1 - self.match_counter]
        res = match_simulator.get_result(team1, team2)

        # the winner of this match advances to the next round
        self.teams_next.append(team1 if res[0] == 1 else team2)
        # the loser goes to the final results
        self.final_results.append(team1 if res[1] == 1 else team2)

        # (team1, team2, result)
        self.matches_history.append( (team1, team2, res) )
        self.match_counter += 1

        if self.match_counter == self.num_teams//2 and self.match_counter == 1:
            # this was the final match, add winner to final results too
            self.final_results.append(self.teams_next[-1])
            # the tournament is over
            return 0

        return -1

    def match_history(self):
        """Print the results of all matches played until today (excluding this days' match)."""
        for match in self.matches_history:
            print("{:>20} {}:{} {}".format(match[0][0], match[2][0], match[2][1], match[1][0]))

    def print_round_matches(self):
        """Print the matches of the actual round."""
        for i in range(self.num_teams//2):
            print("{:>20} VS {}".format(self.teams[i][0], self.teams[self.num_teams - 1 - i][0]))
        print()

    def print_todays_match(self):
        """Print the teams which have been picked to todays' match."""
        x = self.match_counter
        print("{:>20} VS {}".format(self.teams[x][0], self.teams[self.num_teams - 1 - x][0]))
        print()

    def print_final_results(self):
        # print in reversed order so the winner is first in the results
        print("The final results of the tournament:")
        i = 0
        for team in reversed(self.final_results):
            print("{}. {:>4}".format(i+1, team[0]))
            i += 1
        print()
