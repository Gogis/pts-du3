import datetime
import sqlite3
import qualification
import tournament


def main():
    date = datetime.date(2015, 1, 1)
    Q = qualification.Qualification(40, 8, 2)

    res = -1
    while res == -1:
        date += datetime.timedelta(days=1)
        #print(date)
        res = Q.next_day()

    advancers = Q.get_advancers()
    Q.print_groups()

    T = tournament.Tournament(advancers)
    res = -1
    while res == -1:
        date += datetime.timedelta(days=1)
        #print(date, "Following match on the schedule:")
        #T.print_todays_match()
        res = T.next_day()

    T.print_final_results()


if __name__ == "__main__":
    main()
