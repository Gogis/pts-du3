import random

def get_result(team1, team2):
    """'Simulate' match and return a result based on the teams' world rank"""
    # (team_name, team_rank)
    if random.randint(1, team1[2]+team2[2]) <= team1[2]:
    # right now, this only supports 1:0 and 0:1 goal score
        return (1, 0)
    else:
        return (0, 1)
